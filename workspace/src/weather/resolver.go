package weather

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"time"
	"weather/openweather"
)

const (
	errStructToMap = "Erroring Marshalling a Struct to a Map"
)

// WeatherResolverSetter provides an interface
// intended for wraping Open weather API
// Acts as a setter for *Weather
type WeatherResolver interface {
	Timestamp() *time.Time
	Condition() *int
	Main() *string
	Icon() *string
	Description() *string
	Temperature() *float64
	Pressure() *float64
	Humidity() *float64
	WindSpeed() *float64
	WindDirection() *int
	CloudCover() *float64
	RainVolume() *float64
	SnowVolume() *float64

	// Custom Decorators
	TimeFriendly() *string
	TimeOfDay() *string
	Day() *string
}

// Mapper provides behaviour
// for converting a flat struct *no embedded or child structs
// and turning it into a Map
type Mapper interface {
	Map(v interface{}) (m map[string]interface{}, err error)
}

type Uniquer interface {
	Unique(mapper []map[string]interface{}, keys []string) (m []map[string]interface{}, err error)
}

type UniqueMapper interface {
	Collect() []map[string]interface{}
	Mapper
	Uniquer
	Struct(arr []map[string]interface{}) interface{}
}

type UniqueWeather struct{ weather []*Weather }

func NewUniqueWeatherMapper(weather []*Weather) UniqueMapper {
	return UniqueWeather{weather}
}

func (u UniqueWeather) Struct(arr []map[string]interface{}) interface{} {
	collection := make([]*Weather, len(arr))
	for i, v := range arr {
		weather := &Weather{}
		CloudCover := v["cloud_cover"]
		Condition := v["condition"]
		Main := v["main"]
		Description := v["description"]
		Humidity := v["humidity"]
		Icon := v["icon"]
		Pressure := v["pressure"]
		RainVolume := v["rain_volume"]
		SnowVolume := v["snow_volume"]
		Temperature := v["temperature"]
		Timestamp := v["timestamp"]
		WindDirection := v["wind_direction"]
		WindSpeed := v["wind_speed"]

		// Custom Decorators
		Day := v["day"]
		TimeOfDay := v["time_of_day"]
		TimeFriendly := v["time_friendly"]

		if CloudCover != nil {
			ptr := CloudCover.(float64)
			weather.CloudCover = &ptr
		}

		if Condition != nil {
			ptr := int(Condition.(float64))
			weather.Condition = &ptr
		}

		if Main != nil {
			ptr := Main.(string)
			weather.Main = &ptr
		}

		if Description != nil {
			ptr := Description.(string)
			weather.Description = &ptr
		}

		if Humidity != nil {
			ptr := Humidity.(float64)
			weather.Humidity = &ptr
		}

		if Icon != nil {
			ptr := Icon.(string)
			weather.Icon = &ptr
		}

		if Pressure != nil {
			ptr := Pressure.(float64)
			weather.Pressure = &ptr
		}

		if RainVolume != nil {
			ptr := RainVolume.(float64)
			weather.RainVolume = &ptr
		}

		if SnowVolume != nil {
			ptr := SnowVolume.(float64)
			weather.SnowVolume = &ptr
		}

		if Temperature != nil {
			ptr := Temperature.(float64)
			weather.Temperature = &ptr
		}

		if Timestamp != nil {
			layout := "2006-01-02T15:04:05.000Z"
			str := Timestamp.(string)
			ptr, _ := time.Parse(layout, str)
			weather.Dt = &ptr
		}

		if WindDirection != nil {
			ptr := int(WindDirection.(float64))
			weather.WindDirection = &ptr
		}

		if WindSpeed != nil {
			ptr := WindSpeed.(float64)
			weather.WindSpeed = &ptr
		}

		// Custom Decorators
		if Day != nil {
			ptr := Day.(string)
			weather.Day = &ptr
		}

		if TimeOfDay != nil {
			ptr := TimeOfDay.(string)
			weather.TimeOfDay = &ptr
		}

		if TimeFriendly != nil {
			ptr := TimeFriendly.(string)
			weather.TimeFriendly = &ptr
		}

		collection[i] = weather
	}

	return collection
}

func (u UniqueWeather) Collect() []map[string]interface{} {
	var collection = make([]map[string]interface{}, len(u.weather))

	for i, w := range u.weather {
		asMap, err := u.Map(w)

		if err != nil {
			continue
		}

		collection[i] = asMap
	}

	return collection
}

func (u UniqueWeather) Unique(mapper []map[string]interface{}, keys []string) (m []map[string]interface{}, err error) {

	// uniqueIndex
	var ind []int

	invM := make(map[int][]interface{}, 0)

	for i, m := range mapper {
		values := make([]interface{}, len(keys))

		for im := range values {
			values[im] = m[keys[im]]
		}

		invM[i] = values
	}

	// compare each one against each other

	for i := range mapper {
		interm := invM[i]
		ii := i + 1

		if ii < len(mapper) {
			for i2 := range mapper {

				if reflect.DeepEqual(interm, invM[i2]) {
					delete(invM, i2)
				}

			}
		}

		if len(interm) > 0 {
			ind = append(ind, i)
		}
	}

	for _, v := range ind {
		m = append(m, mapper[v])
	}

	return m, nil
}

func (u UniqueWeather) Map(v interface{}) (m map[string]interface{}, err error) {

	js, err := json.Marshal(v)

	if err != nil {
		err = errors.New(errStructToMap)
		return
	}

	err = json.Unmarshal(js, &m)

	return
}

// ForecastResolver provides an interface
// intended for wraping Open weather API
// Acts as a setter for *Forecast
type ForecastResolver interface {
	City() *string
	Weather() []WeatherResolver
}

// ForecastResolve takes a ForecastResolver interface
// serialises a Forecast address and returns its address
func ForecastResolve(resolver ForecastResolver) *Forecast {
	forecast := &Forecast{}
	forecast.City = resolver.City()

	// make a slice to add Weather, set to length of interface
	forecast.Weather = make([]*Weather, len(resolver.Weather()))

	// serialises a concrete Weather Object from it's resolver
	for i, w := range resolver.Weather() {
		forecast.Weather[i] = WeatherResolve(w)
	}

	return forecast
}

// OpenweatherForecastResolver implements ForecastResolver
type OpenweatherForecastResolver struct {
	forecast *openweather.Forecast
}

// NewOpenweatherForecastResolver creates a ForecastResolver
func NewOpenweatherForecastResolver(forecast *openweather.Forecast) ForecastResolver {
	return OpenweatherForecastResolver{forecast}
}

// City - City name
func (r OpenweatherForecastResolver) City() *string {
	return &r.forecast.City.Name
}

// Weather implements array of WeatherResolver
// there are performance implications when using an array of interfaces
// when using an array of interfaces append does not function as it does
// with normal slices.
func (r OpenweatherForecastResolver) Weather() []WeatherResolver {

	// TODO: investigate performance implications of an interface of slices
	// https://github.com/golang/go/wiki/InterfaceSlice

	var resolvers = make([]WeatherResolver, len(r.forecast.Days))
	for i, day := range r.forecast.Days {
		resolvers[i] = OpenweatherWeatherResolver{day}
	}
	return resolvers
}

// WeatherResolve takes a WeatherResolver interface
// serialises a Weather address and returns its address
func WeatherResolve(resolver WeatherResolver) *Weather {
	weather := &Weather{}

	weather.CloudCover = resolver.CloudCover()
	weather.Condition = resolver.Condition()
	weather.Main = resolver.Main()
	weather.Description = resolver.Description()
	weather.Humidity = resolver.Humidity()
	weather.Icon = resolver.Icon()
	weather.Pressure = resolver.Pressure()
	weather.RainVolume = resolver.RainVolume()
	weather.SnowVolume = resolver.SnowVolume()
	weather.Temperature = resolver.Temperature()
	weather.Dt = resolver.Timestamp()
	weather.WindDirection = resolver.WindDirection()
	weather.WindSpeed = resolver.WindSpeed()

	// Custom Decorators
	weather.Day = resolver.Day()
	weather.TimeOfDay = resolver.TimeOfDay()
	weather.TimeFriendly = resolver.TimeFriendly()

	return weather
}

// OpenweatherWeatherResolver implements WeatherResolver
type OpenweatherWeatherResolver struct {
	day openweather.Day
}

// Timestamp - Time of data forecasted
func (r OpenweatherWeatherResolver) Timestamp() *time.Time {
	timeAsInt := r.day.Dt
	timestamp := time.Unix(int64(timeAsInt), 0)

	return &timestamp
}

// Condition - Weather condition id
func (r OpenweatherWeatherResolver) Condition() *int {
	return &r.day.Weather[0].ID
}

// Condition - Weather short description
func (r OpenweatherWeatherResolver) Main() *string {
	return &r.day.Weather[0].Main
}

// Icon - Weather icon id
func (r OpenweatherWeatherResolver) Icon() *string {
	return &r.day.Weather[0].Icon
}

// Description - Weather condition within the group
func (r OpenweatherWeatherResolver) Description() *string {
	return &r.day.Weather[0].Description
}

// Temperature - Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
func (r OpenweatherWeatherResolver) Temperature() *float64 {
	return &r.day.Main.Temp
}

// Pressure - Atmospheric pressure on the sea level by default, hPa
func (r OpenweatherWeatherResolver) Pressure() *float64 {
	return &r.day.Main.Pressure
}

// Humidity - Humidity, %
func (r OpenweatherWeatherResolver) Humidity() *float64 {
	humidity := float64(r.day.Main.Humidity)
	return &humidity
}

// WindSpeed - Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
func (r OpenweatherWeatherResolver) WindSpeed() *float64 {
	return &r.day.Wind.Speed
}

// WindDirection - Wind direction, degrees (meteorological)
func (r OpenweatherWeatherResolver) WindDirection() *int {
	deg := int(r.day.Wind.Deg)
	return &deg
}

// CloudCover - Cloudiness, %
func (r OpenweatherWeatherResolver) CloudCover() *float64 {
	cover := float64(r.day.Clouds.All)
	return &cover
}

// RainVolume - Rain volume for last 3 hours, mm
func (r OpenweatherWeatherResolver) RainVolume() *float64 {
	return &r.day.Rain.ThreeH
}

// SnowVolume - Snow volume for last 3 hours
func (r OpenweatherWeatherResolver) SnowVolume() *float64 {
	return &r.day.Snow.ThreeH
}

/*
* Custom Decorators
 */

// TimeFriendly - Friendly display of time in 24 hour clock
func (r OpenweatherWeatherResolver) TimeFriendly() *string {
	ts := &r.day.Dt
	tm := time.Unix(int64(*ts), 0)
	// only seems to present time on the hour
	ftime := fmt.Sprintf("%v:00", tm.Hour())
	return &ftime
}

// Day - Day of the Week
func (r OpenweatherWeatherResolver) Day() *string {
	ts := &r.day.Dt
	tm := time.Unix(int64(*ts), 0)
	weekday := tm.Weekday().String()
	return &weekday
}

// TimeOfDay - Friendly decription of the part of the day - morning , afterrnoon, night
func (r OpenweatherWeatherResolver) TimeOfDay() *string {
	ts := &r.day.Dt
	tm := time.Unix(int64(*ts), 0)
	hour := tm.Hour()

	if hour < 12 {
		humanise := "Morning"
		return &humanise
	}

	if hour < 17 {
		humanise := "Afternoon"
		return &humanise
	}

	if hour < 24 {
		humanise := "Evening"
		return &humanise
	}

	humanise := "All Day"
	return &humanise
}

type Resolver struct{}

func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

type queryResolver struct{ *Resolver }

// GetForecastByCityName wraps openweather client and makes a call to API
func GetForecastByCityName(city string) (*openweather.MyForecast, error) {
	cli := openweather.NewClient()
	return cli.GetForecastByCityName(city)
}

func (r *queryResolver) WeatherByCity(ctx context.Context, city string, unique []string) (*MyForecast, error) {

	response, err := GetForecastByCityName(city)

	if err != nil {
		return nil, err
	}

	resolver := NewOpenweatherForecastResolver(&response.Forecast)

	forecast := ForecastResolve(resolver)

	// weather mapper

	if len(unique) > 0 {
		mapper := NewUniqueWeatherMapper(forecast.Weather)
		coll := mapper.Collect()
		arr, err2 := mapper.Unique(coll, unique)
		val := mapper.Struct(arr)

		forecast.Weather = val.([]*Weather)

		myforecast := &MyForecast{
			UUID:      response.UUID,
			Timestamp: response.Timestamp,
			Forecast:  forecast,
		}

		return myforecast, err2
	}

	myforecast := &MyForecast{
		UUID:      response.UUID,
		Timestamp: response.Timestamp,
		Forecast:  forecast,
	}

	return myforecast, err
}
