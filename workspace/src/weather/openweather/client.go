package openweather

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

// TODO: you wouldn't have the key in source code in a production app
// const Key = "708c6b9f5408a4985e64bfbad5fe940d"

// Client represents
// the client request to
// openweather api
type Client struct {
	// apiKey     string
	httpClient *http.Client
}

const baseURL string = "https://srv-api-gateway-dot-arbn-232813.appspot.com/gateway"

// NewClient returns the Client struct
func NewClient() *Client {
	var netTransport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}

	return &Client{
		// apiKey: apiKey,
		httpClient: &http.Client{
			Timeout:   10 * time.Second,
			Transport: netTransport,
		},
	}
}

// GetForecastByCityName returns the
// forecast by a given city
// Sample: https://samples.openweathermap.org/data/2.5/forecast?q=London,us&appid=b6907d289e10d714a6e88b30761fae22
func (c *Client) GetForecastByCityName(cityName string) (*MyForecast, error) {
	var forecastData MyForecast
	apiURL := fmt.Sprintf(baseURL+"/forecast?q=%s", cityName)

	err := c.request("GET", apiURL, &forecastData)

	if err != nil {
		return nil, err
	}

	return &forecastData, nil
}

func (c *Client) request(method, url string, data interface{}) error {

	resp, err := c.buildHTTPRequest(method, url)

	if err != nil && resp.Body == nil {
		return err
	}

	defer resp.Body.Close()
	buffer, err := ioutil.ReadAll(resp.Body)

	if err != nil && buffer == nil {
		return err
	}

	return json.Unmarshal(buffer, &data)
}

func (c *Client) buildHTTPRequest(method, url string) (*http.Response, error) {
	request, err := http.NewRequest(method, url, nil)

	if err != nil && request.Body == nil {
		return nil, err
	}
	request.Header.Set("Accept", "application/json")

	return c.httpClient.Do(request)
}
